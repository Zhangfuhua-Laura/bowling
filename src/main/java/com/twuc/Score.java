package com.twuc;

import java.util.ArrayList;
import java.util.List;

public class Score {
    private int totalScore = 0;
    private List<Frame> lineScores = new ArrayList<>();
    private int lastOneThrow;
    private int lastTwoThrow;

    public int getTotalScore() {
        return totalScore;
    }

    public void calculate(){
        for(int index=0; index<lineScores.size(); index++){
            Frame frame = lineScores.get(index);
            totalScore = totalScore + frame.getFirstScore() + frame.getSecondScore();

            calculateSpareBonus(index, frame);

            if(frame.getFirstScore() == 10 && frame.getSecondScore()==0){
                totalScore = totalScore + lineScores.get(index+1).getFirstScore();
                if (lineScores.get(index+1).getFirstScore()==10 && lineScores.get(index+1).getSecondScore()==0)
                    totalScore += lineScores.get(index+1).getFirstScore();
                else
                    totalScore += lineScores.get(index+1).getSecondScore();
            }
        }
    }

    private void calculateSpareBonus(int index, Frame frame) {
        if(index != 9 && frame.getFirstScore() + frame.getSecondScore() == 10)
            totalScore += lineScores.get(index+1).getFirstScore();
        else if(index == 9 && frame.getFirstScore() + frame.getSecondScore() == 10){
            totalScore += lastOneThrow;
        }
    }

    public void setLineScores(List<Frame> lineScores) {
        this.lineScores = lineScores;
    }

    public List<Frame> getLineScores() {
        return lineScores;
    }

    public void setLastOneThrow(int lastOneThrow) {
        this.lastOneThrow = lastOneThrow;
    }

    public void setLastTwoThrow(int lastTwoThrow) {
        this.lastTwoThrow = lastTwoThrow;
    }
}
