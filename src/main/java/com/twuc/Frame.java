package com.twuc;

public class Frame {
    private int firstScore;
    private int secondScore;

    public Frame(int firstScore, int secondScore) {
        this.firstScore = firstScore;
        this.secondScore = secondScore;
    }

    int getFirstScore() {
        return firstScore;
    }

    int getSecondScore() {
        return secondScore;
    }
}
