package com.twuc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BowlingTest {
    private Score score;

    @BeforeEach
    void setUp(){
        score = new Score();
    }

    @Test
    void should_given_no_throws_when_I_get_total_score_then_score_should_be_0(){
        assertEquals(0, score.getTotalScore());
    }

    @Test
    void should_given_1_throws_when_I_get_total_score__then_score_should_be_number_of_pins_knocked_down_of_this_throw(){
        score.setLineScores(Arrays.asList(new Frame(5, 0)));
        score.calculate();
        assertEquals(5, score.getTotalScore());
    }

    @Test
    void should_given_2_throws_when_I_get_total_score_then_score_should_be_total_number_of_pins_knocked_down(){
        score.setLineScores(Collections.singletonList(new Frame(2, 6)));
        score.calculate();
        assertEquals(8, score.getTotalScore());
    }

    @Test
    void should_10_throws_without_strike_or_spare_when_I_get_total_score_then_score_should_be_sum_of_10_frames(){
        score.setLineScores(Arrays.asList(new Frame(1, 1), new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1),new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1), new Frame(2, 2)));
        score.calculate();
        assertEquals(46, score.getTotalScore());
    }

    @Test
    void should_given_first_frames_is_spare_when_I_get_total_score__then_score_should_be_10_plus_spare_bonus(){
        score.setLineScores(Arrays.asList(new Frame(4, 6), new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1),new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1), new Frame(2, 2)));
        score.calculate();
        assertEquals(56, score.getTotalScore());
    }

    @Test
    void should_given_10_frames_with_spare_without_strike_and_the_last_is_not_spare_when_I_get_total_score_then_score_should_plus_spare_bonus(){
        score.setLineScores(Arrays.asList(new Frame(4, 6), new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1),new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(4, 6), new Frame(2, 2)));
        score.calculate();
        assertEquals(66, score.getTotalScore());
    }

    @Test
    void should_given_10_frames_and_the_last_frame_is_spare_and_there_is_no_strike_then_score_should_plus_spare_bonus_of_last_frame(){
        score.setLineScores(Arrays.asList(new Frame(4, 6), new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1),new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(4, 6), new Frame(4, 6)));
        score.setLastOneThrow(2);
        score.calculate();
        assertEquals(76, score.getTotalScore());
    }

    @Test
    void should_given_only_first_frames_is_strike_when_I_get_total_score__then_score_should_be_10_plus_strike_bonus(){
        score.setLineScores(Arrays.asList(new Frame(10, 0), new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1),new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1), new Frame(2, 2)));
        score.calculate();
        assertEquals(60, score.getTotalScore());
    }

    @Test
    void should_given_10_frames_with_strike_and_the_last_is_not_strike_when_I_get_total_score_then_score_should_plus_spare_bonus(){
        score.setLineScores(Arrays.asList(new Frame(4, 6), new Frame(2, 2), new Frame(3, 3), new Frame(4, 4),
                new Frame(1, 1),new Frame(10, 0), new Frame(10, 0), new Frame(4, 4),
                new Frame(4, 6), new Frame(2, 2)));
        score.calculate();
        assertEquals(118, score.getTotalScore());
    }
}
